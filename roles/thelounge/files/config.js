"use strict";
/**
 *  For documentation about the configuration options:
 * 
 *  See https://thelounge.chat/docs/server/configuration
 *  See https://github.com/thelounge/thelounge/blob/master/defaults/config.js
 * 
 */

module.exports = {
	public: false,
	host: 'unix:/tmp/thelounge.socket',
	port: 9000,
	bind: undefined,
	reverseProxy: true,
	theme: "example",
	prefetch: false,
	prefetchStorage: false,
	prefetchMaxImageSize: 2048,
	displayNetwork: true,
	lockNetwork: false,
	useHexIp: false,
	webirc: null,
	logs: {
		format: "YYYY-MM-DD HH:mm:ss",
		timezone: "UTC+00:00",
	},
	maxHistory: 10000,
	defaults: {
		name: "Freenode",
		host: "chat.freenode.net",
		port: 6697,
		password: "",
		tls: true,
		nick: "username",
		username: "username",
		realname: "Real Name",
		join: "#webarch",
	},
	transports: ["polling", "websocket"],
	https: {
		enable: false,
		key: "",
		certificate: "",
		ca: "",
	},
	leaveMessage: "The Lounge",
	identd: {
		enable: false,
		port: 113,
	},
	oidentd: null,
	ldap: {
		enable: false
	},
	debug: {
		ircFramework: false,
		raw: false,
	},
};

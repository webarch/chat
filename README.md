Ansible Playbook to install chat utilities on a debian stretch server:

* Installs [The Lounge](https://github.com/thelounge/thelounge) IRC Web UI
* Running at https://chat.webarchitects.coop/
* Add new users by creating files `roles/thelounge/files/users/` and run the playbook

```
ansible-galaxy install -r requirements.yml
ansible-playbook setup.yml
```
